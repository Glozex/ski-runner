package com.glozex.ski_runner.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.glozex.ski_runner.R;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class ChoiceStylesActivity extends AppCompatActivity {
    String[] type;
    int id;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choicestyles);
        BottomNavigationViewEx bnve = (BottomNavigationViewEx) findViewById(R.id.bnve_bigger_icon);
        bnve.enableAnimation(false);
        bnve.enableShiftingMode(false);
        bnve.enableItemShiftingMode(false);
        bnve.setTextVisibility(false);
        int iconSize = 42;
        bnve.setIconSize(iconSize, iconSize);
        bnve.setItemHeight(BottomNavigationViewEx.dp2px(this, iconSize + 16));
        bnve.setCurrentItem(1);
        bnve.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.ic_main:
                        Intent intent = new Intent(ChoiceStylesActivity.this, WeatherActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        closeActivity();
                        break;
                    case R.id.ic_processingSki:
                        break;
                    case R.id.ic_faq:
                        break;
                    case R.id.ic_settings:
                        intent = new Intent(ChoiceStylesActivity.this, SettingsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case R.id.ic_about:
                        new MaterialDialog.Builder(context)
                                .title(getString(R.string.app_name) + "\n" + "Версия 1.0")
                                .customView(View.inflate(context, R.layout.about_dialog, null), true)
                                .iconRes(R.drawable.ic_logo_skirunner)
                                .positiveText("Закрыть")
                                .show();
                        break;

                }
                return false;
            }
        });
    }

    public void onClick_type(View v) {
        Intent intent = new Intent(this, SkierActivity.class);
        String[] ErrorMessages = getResources().getStringArray(R.array.TitleOintment_RU);

        switch (v.getId()) {
            case R.id.btnclassic:
                id = 1;
                type = new String[] {
                        ErrorMessages[0]
                };
                intent.putExtra("NameOintment", type);
                intent.putExtra("ID", id);
                startActivity(intent);
                break;
            case R.id.btnskate:
                id = 2;
                type = new String[] {
                        ErrorMessages[1]
                        //,ErrorMessages[2]
                };
                intent.putExtra("NameOintment", type);
                intent.putExtra("ID", id);
                startActivity(intent);
                break;
        }
    }

    private void closeActivity() {
        this.finish();
    }
}