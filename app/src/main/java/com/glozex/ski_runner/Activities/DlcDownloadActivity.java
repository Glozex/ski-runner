package com.glozex.ski_runner.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.glozex.ski_runner.R;
import com.glozex.ski_runner.Utility.DownloadLinks;
import com.glozex.ski_runner.Utility.DownloadTask;

public class DlcDownloadActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final String TAG = DlcDownloadActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greeting_installer_dlc);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btndownloadtask:
                if (ContextCompat.checkSelfPermission(DlcDownloadActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                    new DownloadTask(DlcDownloadActivity.this, DownloadLinks.downloadZipUrl);
                }
                else {
                    requestPermissionWithRationale();
                }
                break;
        }
    }

    private void requestPermissionWithRationale() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(DlcDownloadActivity.this.findViewById(R.id.activity_view_dlc), "Приложения запрашивает разрешения." , Snackbar.LENGTH_LONG)
                    .setAction("Включить", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ActivityCompat.requestPermissions(DlcDownloadActivity.this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    public void showNoStoragePermissionSnackbar() {
        Snackbar.make(DlcDownloadActivity.this.findViewById(R.id.activity_view_dlc), "Не получилось получить разрешения." , Snackbar.LENGTH_LONG)
                .setAction("Подробнее", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openApplicationSettings();

                        Toast.makeText(getApplicationContext(),
                                "Пожалуйста, откройте разрешения и разрешите доступ на память.",
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                }).show();
    }

    public void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        startActivityForResult(appSettingsIntent, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                new DownloadTask(DlcDownloadActivity.this, DownloadLinks.downloadZipUrl);
            }
            else {
                showNoStoragePermissionSnackbar();
            }
        }
    }
}