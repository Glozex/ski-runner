package com.glozex.ski_runner.Activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.glozex.ski_runner.Helpers.DataBaseSQLiteWithLoaded;
import com.glozex.ski_runner.R;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.io.File;

public class ReadMoreOintmentActivity extends AppCompatActivity {
    private String SDPath = Environment.getExternalStorageDirectory().getAbsolutePath();

    Context context = this;
    long idOintment;
    ImageView iv;
    TextView nameointment, description, tvprice;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_readmoreointment);
        Intent intent = getIntent();
        File checkFile = new File(String.format("%s/Ski-Runner DLC/%s", SDPath, "images.ch"));

        idOintment = intent.getLongExtra("id", 0);
        iv = (ImageView) findViewById(R.id.imageOintment);
        nameointment = (TextView) findViewById(R.id.NameOintment);
        description = (TextView) findViewById(R.id.Description);
        tvprice = (TextView) findViewById(R.id.OintmentPrice);

        BottomNavigationViewEx bnve = (BottomNavigationViewEx) findViewById(R.id.bnve_bigger_icon3);
        bnve.enableAnimation(false);
        bnve.enableShiftingMode(false);
        bnve.enableItemShiftingMode(false);
        bnve.setTextVisibility(false);
        int iconSize = 42;
        bnve.setIconSize(iconSize, iconSize);
        bnve.setItemHeight(BottomNavigationViewEx.dp2px(this, iconSize + 16));
        bnve.setCurrentItem(1);
        bnve.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.ic_main:
                        Intent intent = new Intent(ReadMoreOintmentActivity.this, WeatherActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        closeActivity();
                        break;
                    case R.id.ic_processingSki:
                        break;
                    case R.id.ic_faq:
                        break;
                    case R.id.ic_settings:
                            intent = new Intent(ReadMoreOintmentActivity.this, SettingsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case R.id.ic_about:
                        new MaterialDialog.Builder(context)
                                .title(getString(R.string.app_name) + "\n" + "Версия 1.0")
                                .customView(View.inflate(context, R.layout.about_dialog, null), true)
                                .iconRes(R.drawable.ic_logo_skirunner)
                                .positiveText("Закрыть")
                                .show();
                        break;

                }
                return false;
            }
        });

        DataBaseSQLiteWithLoaded helper = new DataBaseSQLiteWithLoaded(this);
        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM Ointment JOIN SearchOintments ON Ointment._id = SearchOintments.id_Ointment WHERE _id = " + idOintment, null);
        c.moveToLast();
        String imgid = c.getString(c.getColumnIndex("Image"));
        String title = c.getString(c.getColumnIndex("Title"));
        String price = c.getString(c.getColumnIndex("Price"));
        url = c.getString(c.getColumnIndex("UrlShop"));
        String Description = c.getString(c.getColumnIndex("Description"));

        if (checkFile.exists()) {
            if (imgid != null) {
                File imgFile = new File(String.format("%s/Ski-Runner DLC/%s.jpg", SDPath, imgid));
                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    iv.setImageBitmap(myBitmap);
                } else {
                    iv.setImageResource(R.drawable.ic_launcher_background);
                }
            }
        }
        else
        {
            iv.setVisibility(View.GONE);
        }

        nameointment.setText(title);
        description.setText(Description);
        tvprice.setText(price + " рублей");
        this.setTitle(title);
    }

    public void onClick_result(View v) {
        switch (v.getId()) {
            case R.id.btnshop:
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);
            case R.id.btnback:
                super.finish();
                break;
        }
    }

    private void closeActivity() {
        this.finish();
    }
}