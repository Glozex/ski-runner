package com.glozex.ski_runner.Activities;

import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;

import com.glozex.ski_runner.R;

import java.io.File;

public class SettingsActivity extends AppCompatActivity {

    private static final String TAG = SettingsActivity.class.getSimpleName();
    private String SDPath = Environment.getExternalStorageDirectory().getAbsolutePath();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragmet()).commit();

    }



    private class SettingsFragmet extends PreferenceFragment {
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
            File myFile = new File(SDPath + "/Ski-Runner DLC/images.ch");
            if (myFile.exists()) {
                getPreferenceScreen().findPreference("pdlc").setEnabled(false);
            }
            else {
                getPreferenceScreen().findPreference("pdlc").setEnabled(true);
            }

        }
    }
}
