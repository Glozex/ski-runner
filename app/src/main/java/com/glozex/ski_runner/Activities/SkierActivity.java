package com.glozex.ski_runner.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.afollestad.materialdialogs.MaterialDialog;
import com.glozex.ski_runner.R;
import com.glozex.ski_runner.Utility.AlertDialogBuilder;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.jaygoo.widget.RangeSeekBar;

import java.util.ArrayList;

import fr.ganfra.materialspinner.MaterialSpinner;


public class SkierActivity extends AppCompatActivity {

    private RangeSeekBar SbPrice, SbTemp, SbHumidity;
    private static final String TAG = SkierActivity.class.getSimpleName();
    private static final int PERMISSION_REQUEST_CODE = 1;
    boolean verification = true;
    String[] NameOintment;
    int idActivity;
    EditText EtPriceMin, EtPriceMax, EtTemp, EtHumidity;
    Context context = this;

    ArrayList <AlertDialogBuilder> ClassMessageList = new ArrayList <> ();
    ToggleButton type_snow_dry, type_snow_wet, type_snow_new, type_snow_old, type_snow_transformed, type_snow_rigid, type_snow_contaminated;

    SharedPreferences sPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_style_course);

        Intent intent = getIntent();
        NameOintment = intent.getStringArrayExtra("NameOintment");
        idActivity = intent.getIntExtra("ID", 0);
        SetTitles(idActivity);
        type_snow_dry = (ToggleButton) findViewById(R.id.t_btn_typesnow_dry);
        type_snow_wet = (ToggleButton) findViewById(R.id.t_btn_typesnow_wet);
        type_snow_new = (ToggleButton) findViewById(R.id.t_btn_structureshow_new);
        type_snow_old = (ToggleButton) findViewById(R.id.t_btn_structureshow_old);
        type_snow_transformed = (ToggleButton) findViewById(R.id.t_btn_structureshow_transformed);
        type_snow_rigid = (ToggleButton) findViewById(R.id.t_btn_typesnow_RigidSnow);
        type_snow_contaminated = (ToggleButton) findViewById(R.id.t_btn_typesnow_ContaminatedSnow);

        type_snow_dry.setOnCheckedChangeListener(changeChecker);
        type_snow_wet.setOnCheckedChangeListener(changeChecker);
        type_snow_new.setOnCheckedChangeListener(changeChecker);
        type_snow_old.setOnCheckedChangeListener(changeChecker);
        type_snow_transformed.setOnCheckedChangeListener(changeChecker);
        type_snow_rigid.setOnCheckedChangeListener(changeChecker);
        type_snow_contaminated.setOnCheckedChangeListener(changeChecker);

        EtPriceMin = (EditText)findViewById(R.id.EtPriceMin);
        EtPriceMax = (EditText)findViewById(R.id.EtPriceMax);
        EtTemp = (EditText)findViewById(R.id.EtTemp);
        EtHumidity = (EditText)findViewById(R.id.EtHumidity);

        SbPrice = (RangeSeekBar)findViewById(R.id.SbPrice);
        SbTemp = (RangeSeekBar)findViewById(R.id.SbTemp);
        SbHumidity = (RangeSeekBar)findViewById(R.id.SbHumidity);
        SbPrice.setValue(0,6000);
        SbTemp.setValue(Float.parseFloat(loadSettings("HOUR_TEMPERATURE", "-50")));
        SbHumidity.setValue(Float.parseFloat(loadSettings("HOUR_HUMIDITY", "0")));
        EtTemp.setText(loadSettings("HOUR_TEMPERATURE", "-50"));
        EtHumidity.setText(loadSettings("HOUR_HUMIDITY", "0"));
        BottomNavigationViewEx bnve = (BottomNavigationViewEx) findViewById(R.id.bnve_bigger_icon2);
        bnve.enableAnimation(false);
        bnve.enableShiftingMode(false);
        bnve.enableItemShiftingMode(false);
        bnve.setTextVisibility(false);
        int iconSize = 42;
        bnve.setIconSize(iconSize, iconSize);
        bnve.setItemHeight(BottomNavigationViewEx.dp2px(this, iconSize + 16));
        bnve.setCurrentItem(1);

        bnve.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.ic_main:
                        Intent intent = new Intent(SkierActivity.this, WeatherActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        closeActivity();
                        break;
                    case R.id.ic_processingSki:
                        break;
                    case R.id.ic_faq:
                        break;
                    case R.id.ic_settings:
                        intent = new Intent(SkierActivity.this, SettingsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case R.id.ic_about:
                        new MaterialDialog.Builder(context)
                                .title(getString(R.string.app_name) + "\n" + "Версия 1.0")
                                .customView(View.inflate(context, R.layout.about_dialog, null), true)
                                .iconRes(R.drawable.ic_logo_skirunner)
                                .positiveText("Закрыть")
                                .show();
                        break;

                }
                return false;
            }
        });

        ArrayAdapter <String> adapter = new ArrayAdapter < String > (this, android.R.layout.simple_spinner_item, NameOintment);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        MaterialSpinner spinner = (MaterialSpinner) findViewById(R.id.s_ointment);
        spinner.setAdapter(adapter);
        spinner.setSelection(1);

        ClassMessageListAdd();

      SbPrice.setOnRangeChangedListener(new RangeSeekBar.OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar view, float min, float max, boolean isFromUser) {
                if (isFromUser) {

                    EtPriceMin.setText(String.valueOf((int)min));
                    EtPriceMax.setText(String.valueOf((int)max));
                }
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {}

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {}
        });

      SbTemp.setOnRangeChangedListener(new RangeSeekBar.OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar view, float min, float max, boolean isFromUser) {
                if (isFromUser) {
                    EtTemp.setText(String.valueOf((int) min));
                }
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {}

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {}
        });

      SbHumidity.setOnRangeChangedListener(new RangeSeekBar.OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar view, float min, float max, boolean isFromUser) {
                if (isFromUser) {
                    EtHumidity.setText(String.valueOf((int) min));
                }
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {}

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {}
        });

      EtPriceMin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    SbPrice.setValue(Integer.parseInt(s.toString()), Integer.parseInt(EtPriceMax.getText().toString()));
                } catch(Exception ex) {}
            }
        });

      EtPriceMax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    SbPrice.setValue(Integer.parseInt(EtPriceMin.getText().toString()),Integer.parseInt(s.toString()));
                } catch(Exception ex) {}
            }
        });

      EtTemp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    SbTemp.setValue(Integer.parseInt(s.toString()));
                } catch(Exception ex) {}
            }
        });

      EtHumidity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    SbHumidity.setValue(Integer.parseInt(s.toString()));
                } catch(Exception ex) {}
            }
        });
    }

    public void SetTitles(int id) {
        if (id == 1) {
            this.setTitle(getString(R.string.app_name_ClassicSkierActivity));
        } else {
            this.setTitle(getString(R.string.app_name_ClierSkateActivity));
        }
    }

    public void ClassMessageListAdd() {
        String[] ErrorMessages = getResources().getStringArray(R.array.Error_Messages);

        for (String ErrorM: ErrorMessages) {
            ClassMessageList.add(new AlertDialogBuilder("Ошибка", ErrorM, this));
        }
    }

    CompoundButton.OnCheckedChangeListener changeChecker = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                if (buttonView == type_snow_dry) {
                    type_snow_wet.setEnabled(false);
                    type_snow_wet.setAlpha(.5f);
                    type_snow_new.setEnabled(false);
                    type_snow_new.setAlpha(.5f);
                    type_snow_old.setEnabled(false);
                    type_snow_old.setAlpha(.5f);
                    type_snow_transformed.setEnabled(false);
                    type_snow_transformed.setAlpha(.5f);
                    type_snow_rigid.setEnabled(false);
                    type_snow_rigid.setAlpha(.5f);
                    type_snow_contaminated.setEnabled(false);
                    type_snow_contaminated.setAlpha(.5f);

                }
                if (buttonView == type_snow_wet) {
                    type_snow_dry.setEnabled(false);
                    type_snow_dry.setAlpha(.5f);
                    type_snow_new.setEnabled(false);
                    type_snow_new.setAlpha(.5f);
                    type_snow_old.setEnabled(false);
                    type_snow_old.setAlpha(.5f);
                    type_snow_transformed.setEnabled(false);
                    type_snow_transformed.setAlpha(.5f);
                    type_snow_rigid.setEnabled(false);
                    type_snow_rigid.setAlpha(.5f);
                    type_snow_contaminated.setEnabled(false);
                    type_snow_contaminated.setAlpha(.5f);
                }
                if (buttonView == type_snow_new) {
                    type_snow_dry.setEnabled(false);
                    type_snow_dry.setAlpha(.5f);
                    type_snow_wet.setEnabled(false);
                    type_snow_wet.setAlpha(.5f);
                    type_snow_old.setEnabled(false);
                    type_snow_old.setAlpha(.5f);
                    type_snow_transformed.setEnabled(false);
                    type_snow_transformed.setAlpha(.5f);
                    type_snow_rigid.setEnabled(false);
                    type_snow_rigid.setAlpha(.5f);
                    type_snow_contaminated.setEnabled(false);
                    type_snow_contaminated.setAlpha(.5f);
                }
                if (buttonView == type_snow_old) {
                    type_snow_dry.setEnabled(false);
                    type_snow_dry.setAlpha(.5f);
                    type_snow_new.setEnabled(false);
                    type_snow_new.setAlpha(.5f);
                    type_snow_wet.setEnabled(false);
                    type_snow_wet.setAlpha(.5f);
                    type_snow_transformed.setEnabled(false);
                    type_snow_transformed.setAlpha(.5f);
                    type_snow_rigid.setEnabled(false);
                    type_snow_rigid.setAlpha(.5f);
                    type_snow_contaminated.setEnabled(false);
                    type_snow_contaminated.setAlpha(.5f);
                }
                if (buttonView == type_snow_transformed) {
                    type_snow_dry.setEnabled(false);
                    type_snow_dry.setAlpha(.5f);
                    type_snow_new.setEnabled(false);
                    type_snow_new.setAlpha(.5f);
                    type_snow_old.setEnabled(false);
                    type_snow_old.setAlpha(.5f);
                    type_snow_wet.setEnabled(false);
                    type_snow_wet.setAlpha(.5f);
                    type_snow_rigid.setEnabled(false);
                    type_snow_rigid.setAlpha(.5f);
                    type_snow_contaminated.setEnabled(false);
                    type_snow_contaminated.setAlpha(.5f);
                }
                if (buttonView == type_snow_rigid) {
                    type_snow_dry.setEnabled(false);
                    type_snow_dry.setAlpha(.5f);
                    type_snow_new.setEnabled(false);
                    type_snow_new.setAlpha(.5f);
                    type_snow_old.setEnabled(false);
                    type_snow_old.setAlpha(.5f);
                    type_snow_wet.setEnabled(false);
                    type_snow_wet.setAlpha(.5f);
                    type_snow_transformed.setEnabled(false);
                    type_snow_transformed.setAlpha(.5f);
                    type_snow_contaminated.setEnabled(false);
                    type_snow_contaminated.setAlpha(.5f);
                }
                if (buttonView == type_snow_contaminated) {
                    type_snow_dry.setEnabled(false);
                    type_snow_dry.setAlpha(.5f);
                    type_snow_new.setEnabled(false);
                    type_snow_new.setAlpha(.5f);
                    type_snow_old.setEnabled(false);
                    type_snow_old.setAlpha(.5f);
                    type_snow_wet.setEnabled(false);
                    type_snow_wet.setAlpha(.5f);
                    type_snow_rigid.setEnabled(false);
                    type_snow_rigid.setAlpha(.5f);
                    type_snow_transformed.setEnabled(false);
                    type_snow_transformed.setAlpha(.5f);
                }
            } else {
                if (buttonView == type_snow_dry) {
                    type_snow_wet.setEnabled(true);
                    type_snow_wet.setAlpha(1f);
                    type_snow_old.setEnabled(true);
                    type_snow_old.setAlpha(1f);
                    type_snow_new.setEnabled(true);
                    type_snow_new.setAlpha(1f);
                    type_snow_transformed.setEnabled(true);
                    type_snow_transformed.setAlpha(1f);
                    type_snow_rigid.setEnabled(true);
                    type_snow_rigid.setAlpha(1f);
                    type_snow_contaminated.setEnabled(true);
                    type_snow_contaminated.setAlpha(1f);
                }
                if (buttonView == type_snow_wet) {
                    type_snow_dry.setEnabled(true);
                    type_snow_dry.setAlpha(1f);
                    type_snow_old.setEnabled(true);
                    type_snow_old.setAlpha(1f);
                    type_snow_new.setEnabled(true);
                    type_snow_new.setAlpha(1f);
                    type_snow_transformed.setEnabled(true);
                    type_snow_transformed.setAlpha(1f);
                    type_snow_rigid.setEnabled(true);
                    type_snow_rigid.setAlpha(1f);
                    type_snow_contaminated.setEnabled(true);
                    type_snow_contaminated.setAlpha(1f);
                }
                if (buttonView == type_snow_new) {
                    type_snow_wet.setEnabled(true);
                    type_snow_wet.setAlpha(1f);
                    type_snow_old.setEnabled(true);
                    type_snow_old.setAlpha(1f);
                    type_snow_dry.setEnabled(true);
                    type_snow_dry.setAlpha(1f);
                    type_snow_transformed.setEnabled(true);
                    type_snow_transformed.setAlpha(1f);
                    type_snow_rigid.setEnabled(true);
                    type_snow_rigid.setAlpha(1f);
                    type_snow_contaminated.setEnabled(true);
                    type_snow_contaminated.setAlpha(1f);
                }
                if (buttonView == type_snow_old) {
                    type_snow_wet.setEnabled(true);
                    type_snow_wet.setAlpha(1f);
                    type_snow_dry.setEnabled(true);
                    type_snow_dry.setAlpha(1f);
                    type_snow_new.setEnabled(true);
                    type_snow_new.setAlpha(1f);
                    type_snow_transformed.setEnabled(true);
                    type_snow_transformed.setAlpha(1f);
                    type_snow_rigid.setEnabled(true);
                    type_snow_rigid.setAlpha(1f);
                    type_snow_contaminated.setEnabled(true);
                    type_snow_contaminated.setAlpha(1f);
                }
                if (buttonView == type_snow_transformed) {
                    type_snow_wet.setEnabled(true);
                    type_snow_wet.setAlpha(1f);
                    type_snow_old.setEnabled(true);
                    type_snow_old.setAlpha(1f);
                    type_snow_new.setEnabled(true);
                    type_snow_new.setAlpha(1f);
                    type_snow_dry.setEnabled(true);
                    type_snow_dry.setAlpha(1f);
                    type_snow_rigid.setEnabled(true);
                    type_snow_rigid.setAlpha(1f);
                    type_snow_contaminated.setEnabled(true);
                    type_snow_contaminated.setAlpha(1f);
                }
                if (buttonView == type_snow_rigid) {
                    type_snow_wet.setEnabled(true);
                    type_snow_wet.setAlpha(1f);
                    type_snow_old.setEnabled(true);
                    type_snow_old.setAlpha(1f);
                    type_snow_new.setEnabled(true);
                    type_snow_new.setAlpha(1f);
                    type_snow_dry.setEnabled(true);
                    type_snow_dry.setAlpha(1f);
                    type_snow_transformed.setEnabled(true);
                    type_snow_transformed.setAlpha(1f);
                    type_snow_contaminated.setEnabled(true);
                    type_snow_contaminated.setAlpha(1f);
                }
                if (buttonView == type_snow_contaminated) {
                    type_snow_wet.setEnabled(true);
                    type_snow_wet.setAlpha(1f);
                    type_snow_old.setEnabled(true);
                    type_snow_old.setAlpha(1f);
                    type_snow_new.setEnabled(true);
                    type_snow_new.setAlpha(1f);
                    type_snow_dry.setEnabled(true);
                    type_snow_dry.setAlpha(1f);
                    type_snow_rigid.setEnabled(true);
                    type_snow_rigid.setAlpha(1f);
                    type_snow_transformed.setEnabled(true);
                    type_snow_transformed.setAlpha(1f);
                }

            }
        }
    };

    public void onClick_result(View v) {
        switch (v.getId()) {
            case R.id.btnresult:
                if (ContextCompat.checkSelfPermission(SkierActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    MaterialSpinner spinner = (MaterialSpinner) findViewById(R.id.s_ointment);
                    EtPriceMin = (EditText) findViewById(R.id.EtPriceMin);
                    EtPriceMax = (EditText) findViewById(R.id.EtPriceMax);
                    EtTemp = (EditText) findViewById(R.id.EtTemp);
                    EtHumidity = (EditText) findViewById(R.id.EtHumidity);

                    Intent intent = new Intent(this, ResultActivity.class);
                    if (verification) {
                        if (spinner.getSelectedItemPosition() == 0) {
                            ClassMessageList.get(0).ConclusionAlertDialog();
                            break;
                        }
                        if (!type_snow_dry.isChecked() && !type_snow_wet.isChecked() && !type_snow_new.isChecked() && !type_snow_rigid.isChecked() && !type_snow_contaminated.isChecked() && !type_snow_old.isChecked() && !type_snow_transformed.isChecked()) {
                            ClassMessageList.get(1).ConclusionAlertDialog();
                            break;
                        }
                    }

                    if (type_snow_dry.isChecked()) {
                        intent.putExtra("type_snow", "DrySnow");
                    }
                    if (type_snow_wet.isChecked()) {
                        intent.putExtra("type_snow", "WetSnow");
                    }
                    if (type_snow_new.isChecked()) {
                        intent.putExtra("type_snow", "NewSnow");
                    }
                    if (type_snow_old.isChecked()) {
                        intent.putExtra("type_snow", "OldSnow");
                    }
                    if (type_snow_transformed.isChecked()) {
                        intent.putExtra("type_snow", "TransformedSnow");
                    }
                    if (type_snow_rigid.isChecked()) {
                        intent.putExtra("type_snow", "RigidSnow");
                    }

                    if (type_snow_contaminated.isChecked()) {
                        intent.putExtra("type_snow", "ContaminatedSnow");
                    }

                    intent.putExtra("Spinner", spinner.getSelectedItem().toString());
                    intent.putExtra("PriceMin", EtPriceMin.getText().toString());
                    intent.putExtra("PriceMax", EtPriceMax.getText().toString());
                    intent.putExtra("temperature", EtTemp.getText().toString());
                    intent.putExtra("humidity", EtHumidity.getText().toString());

                    startActivity(intent);
                } else {
                    requestPermissionWithRationale();
                }
                break;
            case R.id.btnhelp:
                break;
        }
    }

    private String loadSettings(String key, String defValue) {
        sPref = getSharedPreferences("WeatherPreferences", MODE_PRIVATE);
        String saved = sPref.getString(key, defValue);
        Log.i (TAG, "SharedPreferencesLoad: " + key + ": OK");
        return saved;
    }

    private void requestPermissionWithRationale() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(SkierActivity.this.findViewById(R.id.activity_view_course), "Приложения запрашивает разрешения." , Snackbar.LENGTH_LONG)
                    .setAction("Включить", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ActivityCompat.requestPermissions(SkierActivity.this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    public void showNoStoragePermissionSnackbar() {
        Snackbar.make(SkierActivity.this.findViewById(R.id.activity_view_course), "Не получилось получить разрешения." , Snackbar.LENGTH_LONG)
                .setAction("Подробнее", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openApplicationSettings();

                        Toast.makeText(getApplicationContext(),
                                "Пожалуйста, откройте разрешения и разрешите доступ на память.",
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                }).show();
    }

    public void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        startActivityForResult(appSettingsIntent, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) { }
            else {
                showNoStoragePermissionSnackbar();
            }
        }
    }

    private void closeActivity() {
        this.finish();
    }
}