package com.glozex.ski_runner.Activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.glozex.ski_runner.Adapters.CustomAdapterListview;
import com.glozex.ski_runner.Helpers.DataBaseSQLiteWithLoaded;
import com.glozex.ski_runner.R;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class ResultActivity extends AppCompatActivity {

    private static final String TAG = ResultActivity.class.getSimpleName();

    int temperature, humidity, pricemin, pricemax;
    String type_snow, NameOintment;
    ListView lv;
    CustomAdapterListview adapter;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        String[] TitleOintmentRU = getResources().getStringArray(R.array.TitleOintment_RU);
        String[] TitleOintmentEN = getResources().getStringArray(R.array.TitleOintment_EN);
        Intent intent = getIntent();
        NameOintment = intent.getStringExtra("Spinner");
        temperature = Integer.parseInt(intent.getStringExtra("temperature"));
        humidity = Integer.parseInt(intent.getStringExtra("humidity"));
        pricemax = Integer.parseInt(intent.getStringExtra("PriceMax"));
        pricemin = Integer.parseInt(intent.getStringExtra("PriceMin"));
        type_snow = intent.getStringExtra("type_snow");

        BottomNavigationViewEx bnve = (BottomNavigationViewEx) findViewById(R.id.bnve_bigger_icon2);
        bnve.enableAnimation(false);
        bnve.enableShiftingMode(false);
        bnve.enableItemShiftingMode(false);
        bnve.setTextVisibility(false);
        int iconSize = 42;
        bnve.setIconSize(iconSize, iconSize);
        bnve.setItemHeight(BottomNavigationViewEx.dp2px(this, iconSize + 16));
        bnve.setCurrentItem(1);

        bnve.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.ic_main:
                        Intent intent = new Intent(ResultActivity.this, WeatherActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        closeActivity();
                        break;
                    case R.id.ic_processingSki:
                        break;
                    case R.id.ic_faq:
                        break;
                    case R.id.ic_settings:
                        intent = new Intent(ResultActivity.this, SettingsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case R.id.ic_about:
                        new MaterialDialog.Builder(context)
                                .title(getString(R.string.app_name) + "\n" + "Версия 1.0")
                                .customView(View.inflate(context, R.layout.about_dialog, null), true)
                                .iconRes(R.drawable.ic_logo_skirunner)
                                .positiveText("Закрыть")
                                .show();
                        break;

                }
                return false;
            }
        });

        for (int i = 0; i < TitleOintmentRU.length; i++) {
            if (NameOintment.equals(TitleOintmentRU[i])) {
                NameOintment = TitleOintmentEN[i];
            }
        }

        Log.i(TAG, "NameOintment: " + NameOintment);
        Log.i(TAG, "temperature: " + temperature);
        Log.i(TAG, "humidity: " + humidity);
        Log.i(TAG, "type_snow: " + type_snow);
        Log.i(TAG, "PriceMax: " + pricemax);
        Log.i(TAG, "PriceMin: " + pricemin);

        DataBaseSQLiteWithLoaded helper = new DataBaseSQLiteWithLoaded(this);
        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor c = db.rawQuery(generateSQL(NameOintment, temperature, humidity, type_snow, pricemin, pricemax), null);
        lv = (ListView) findViewById(R.id.list_of_ointments);
        String[] fields = new String[] {
                "Brand",
                "Title",
                "Price",
                "Image"
        };
        int[] views = new int[] {
                R.id.lv_manufacturers, R.id.lv_ointment, R.id.lv_price, R.id.lv_image
        };

        adapter = new CustomAdapterListview(this, R.layout.list_of_ointments, c, fields, views, 0);
        lv.setAdapter(adapter);
        lv.setEmptyView(findViewById(R.id.emptylvText));
        lv.setEmptyView(findViewById(R.id.emptylvImage));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            public void onItemClick(AdapterView<?> parent, View view, int position, long id_Ointment){
                Intent intent = new Intent(context, ReadMoreOintmentActivity.class);
                intent.putExtra("id", id_Ointment);
                //intent.putExtra("ID", id);
                startActivity(intent);
                Log.i(TAG,"setOnItemClickListener: " + id_Ointment );
            }
        });

        Log.i(TAG, "SQL: " + generateSQL(NameOintment, temperature, humidity, type_snow, pricemin, pricemax));
    }

    public String generateSQL(String NameOintment, int temperature, int humidity, String type_snow, int pricemin, int pricemax) {
        String SQLRequest;
        SQLRequest = "SELECT * FROM SearchOintments " +
                "JOIN Ointment ON SearchOintments.id_Ointment = Ointment._id " +
                "JOIN Brand ON Ointment.id_Brand = Brand.id " +
                "WHERE NameOintment = '"+ NameOintment +"' AND TemperatureMin <= "+ temperature +" AND TemperatureMax >= "+ temperature +" " +
                "AND TypeSnow = '"+ type_snow +"' " +
                "AND "+ pricemin +" < Price AND Price < "+ pricemax +" " +
                "AND HumidityMin <= "+ humidity +" AND HumidityMax >= "+ humidity +";";
        return SQLRequest;
    }


    private void closeActivity() {
        this.finish();
    }
}