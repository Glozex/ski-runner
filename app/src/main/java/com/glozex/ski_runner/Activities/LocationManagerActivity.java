package com.glozex.ski_runner.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.glozex.ski_runner.Helpers.DataBaseSQLiteWithLoaded;
import com.glozex.ski_runner.R;
import com.glozex.ski_runner.Utility.GetLocationKeyGeolocation;

public class LocationManagerActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final String TAG = LocationManagerActivity.class.getSimpleName();
    ListView lv;
    SimpleCursorAdapter adapter;
    Context context = this;
    SharedPreferences sPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locationmanager);

        DataBaseSQLiteWithLoaded helper = new DataBaseSQLiteWithLoaded(this);
        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM LocationCities", null);
        lv = (ListView) findViewById(R.id.list_of_ointments);

        String[] fields = new String[] {"CityName"};
        int[] views = new int[]{ R.id.lv_NameCity};
        adapter = new SimpleCursorAdapter(this, R.layout.list_cities, c, fields, views, 0);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                DataBaseSQLiteWithLoaded helper = new DataBaseSQLiteWithLoaded(context);
                SQLiteDatabase db = helper.getWritableDatabase();
                Cursor c = db.rawQuery("SELECT * FROM LocationCities WHERE _id = " + id, null);
                c.moveToLast();
                saveLocationKey(c.getString(c.getColumnIndex("KeyCity")),c.getString(c.getColumnIndex("CityName")));
                Intent intent = new Intent(context, WeatherActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                ((Activity)context).finish();
                Log.i(TAG,"setOnItemClickListener: " + id );
            }
        });


        lv.setAdapter(adapter);
    }

    public void onClick_gps(View v) {
        switch (v.getId()) {
            case R.id.btnGetGeoposition:
                if (ContextCompat.checkSelfPermission(LocationManagerActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                    (new GetLocationKeyGeolocation(LocationManagerActivity.this)).execute();
                }
                else {
                    requestPermissionWithRationale();
                }
                break;
        }
    }

    private void requestPermissionWithRationale() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Snackbar.make(LocationManagerActivity.this.findViewById(R.id.activity_view), "Приложения запрашивает разрешения." , Snackbar.LENGTH_LONG)
                    .setAction("Включить", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ActivityCompat.requestPermissions(LocationManagerActivity.this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    public void showNoStoragePermissionSnackbar() {
        Snackbar.make(LocationManagerActivity.this.findViewById(R.id.activity_view), "Не получилось получить разрешения." , Snackbar.LENGTH_LONG)
                .setAction("Подробнее", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openApplicationSettings();

                        Toast.makeText(getApplicationContext(),
                                "Пожалуйста, откройте разрешения и разрешите получить данные о вашем местоположении.",
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                }).show();
    }

    public void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        startActivityForResult(appSettingsIntent, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                (new GetLocationKeyGeolocation(LocationManagerActivity.this)).execute();
            }
            else {
                showNoStoragePermissionSnackbar();
            }
        }
    }

    private void saveLocationKey(String key, String city) {
        sPref = context.getSharedPreferences("MainPreferences", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("KEY_CITY", key);
        ed.putString("NAME_CITY", city);
        ed.apply();
        Log.i (TAG, "SharedPreferences: saveLocationKey: " + sPref.getString("KEY_CITY", ""));
    }
}

