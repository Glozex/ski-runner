package com.glozex.ski_runner.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.glozex.ski_runner.Helpers.GetHTTPDataHelper;
import com.glozex.ski_runner.R;
import com.glozex.ski_runner.Utility.AlertDialogBuilder;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class WeatherActivity extends AppCompatActivity {

    private static final String TAG = WeatherActivity.class.getSimpleName();
    ArrayList<TextView> tvW=new ArrayList<TextView>();
    ArrayList<ImageView> ivW=new ArrayList<ImageView>();
    private String locationkey, cityname, firstrunapp;
    TextView tvCity, tvDayOfTheWeek, tvRealTime, tvHourWeather, tvHourHumidity, tvHourWind, tvHourTemp, tvWeatherDayOne,
            tvWeatherDayTwo, tvWeatherDayThree, tvTempDayOne,tvTempDayTwo,tvTempDayThree,  tvDayOne,tvDayTwo, tvDayThree;
    ImageView ivDayOne, ivDayTwo, ivDayThree;
    String URLForecasts = "http://dataservice.accuweather.com/forecasts/v1";

    SharedPreferences sPref;
    Context context = this;
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_weather);

        tvCity = (TextView) findViewById(R.id.tvCity);
        tvDayOfTheWeek = (TextView) findViewById(R.id.tvDayOfTheWeek);
        tvRealTime = (TextView) findViewById(R.id.tvRealTime);
        tvHourWeather = (TextView) findViewById(R.id.tvHourWeather);
        tvHourHumidity = (TextView) findViewById(R.id.tvHourHumidity);
        tvHourWind = (TextView) findViewById(R.id.tvHourWind);
        tvHourTemp = (TextView) findViewById(R.id.tvHourTemp);


        tvWeatherDayOne = (TextView) findViewById(R.id.tvWeatherDay_1);
        tvWeatherDayTwo = (TextView) findViewById(R.id.tvWeatherDay_2);
        tvWeatherDayThree = (TextView) findViewById(R.id.tvWeatherDay_3);
        tvTempDayOne = (TextView) findViewById(R.id.tvTempDay_1);
        tvTempDayTwo = (TextView) findViewById(R.id.tvTempDay_2);
        tvTempDayThree = (TextView) findViewById(R.id.tvTempDay_3);
        tvDayOne = (TextView) findViewById(R.id.tvDay_1);
        tvDayTwo = (TextView) findViewById(R.id.tvDay_2);
        tvDayThree = (TextView) findViewById(R.id.tvDay_3);
        ivDayOne = (ImageView) findViewById(R.id.ivDay_1);
        ivDayTwo = (ImageView) findViewById(R.id.ivDay_2);
        ivDayThree = (ImageView) findViewById(R.id.ivDay_3);

        tvW.add(tvWeatherDayOne);
        tvW.add(tvWeatherDayTwo);
        tvW.add(tvWeatherDayThree);
        tvW.add(tvTempDayOne);
        tvW.add(tvTempDayTwo);
        tvW.add(tvTempDayThree);
        tvW.add(tvDayOne);
        tvW.add(tvDayTwo);
        tvW.add(tvDayThree);
        ivW.add(ivDayOne);
        ivW.add(ivDayTwo);
        ivW.add(ivDayThree);

        BottomNavigationViewEx bnve = (BottomNavigationViewEx) findViewById(R.id.bnve_bigger_icon2);
        bnve.enableAnimation(false);
        bnve.enableShiftingMode(false);
        bnve.enableItemShiftingMode(false);
        bnve.setTextVisibility(false);
        bnve.setIconSize(42, 42);
        bnve.setItemHeight(BottomNavigationViewEx.dp2px(this, 42 + 16));
        bnve.setCurrentItem(0);

        bnve.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.ic_main:
                        break;
                    case R.id.ic_processingSki:
                        Intent intent = new Intent(WeatherActivity.this, ChoiceStylesActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        closeActivity();
                        break;
                    case R.id.ic_faq:
                        break;
                    case R.id.ic_settings:
                        intent = new Intent(WeatherActivity.this, SettingsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case R.id.ic_about:
                        new MaterialDialog.Builder(context)
                                .title(getString(R.string.app_name) + "\n" + "Версия 1.0")
                                .customView(View.inflate(context, R.layout.about_dialog, null), true)
                                .iconRes(R.drawable.ic_logo_skirunner)
                                .positiveText("Закрыть")
                                .show();
                        break;

                }
                return false;
            }
        });

        locationkey = loadSettings("KEY_CITY", "");
        cityname = loadSettings("NAME_CITY", "Погода");
        firstrunapp = loadSettings("FIRSTRUNAPP", "0");
        this.setTitle(cityname);

        if (locationkey.isEmpty()) {
            Intent intent = new Intent(WeatherActivity.this, LocationManagerActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            closeActivity();
        } else {
            if (Integer.parseInt(firstrunapp) == 0) {
                new GetWeather().execute();
                saveLocationKey("FIRSTRUNAPP", "1");
            }
            // new GetWeather().execute();
        }


        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swifeRefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetWeather().execute();
            }
        });

        weatherRecovery();
    }

    private class GetWeather extends AsyncTask<URL,Void,String[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            if(result[0] != null && !result[0].equals("") && result[1] != null && !result[1].equals("")) {
                try {
                    parseJSON(result[0], result[1]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                new AlertDialogBuilder("Ошибка", getResources().getString(R.string.Error_Messages_Json_Weather), context).ConclusionAlertDialog();
            }
            mSwipeRefreshLayout.setRefreshing(false);
        }

        @Override
        protected String[] doInBackground(URL... urls) {
            String[] result = new String[2];
            GetHTTPDataHelper FiveDaysOfDailyForecasts = new GetHTTPDataHelper();
            GetHTTPDataHelper OneHourOfHourlyForecasts = new GetHTTPDataHelper();
            FiveDaysOfDailyForecasts.setWeatherBaseUrl(URLForecasts);
            URL weatherUrlFiveDaysOfDailyForecasts = FiveDaysOfDailyForecasts.buildURLForWeather("daily", "5day", locationkey);
            URL weatherUrlOneHourOfHourlyForecasts = FiveDaysOfDailyForecasts.buildURLForWeather("hourly", "1hour", locationkey);
            Log.i (TAG, "doInBackground: weatherUrlFiveDaysOfDailyForecasts: " + weatherUrlFiveDaysOfDailyForecasts);
            Log.i (TAG, "doInBackground: weatherUrlOneHourOfHourlyForecasts: " + weatherUrlOneHourOfHourlyForecasts);
            result[0] = FiveDaysOfDailyForecasts.getHTTPData(weatherUrlFiveDaysOfDailyForecasts);
            result[1] = FiveDaysOfDailyForecasts.getHTTPData(weatherUrlOneHourOfHourlyForecasts);
            Log.i (TAG, "doInBackground: JSONFiveDaysOfDailyForecasts: " + result[0]);
            Log.i (TAG, "doInBackground: JSONOneHourOfHourlyForecasts: " + result[1]);
            Log.i (TAG, "doInBackground: locationkey: " + locationkey);
            return result;
        }
    }

    private void parseJSON(String JSONFiveDaysOfDailyForecasts, String JSONOneHourOfHourlyForecasts) throws ParseException {
        try {
            sPref = getSharedPreferences("WeatherPreferences", MODE_PRIVATE);
            JSONArray weatherJsonArray = new JSONArray(JSONOneHourOfHourlyForecasts);


            for (int i = 0; i < 1; i++) {
                JSONObject resultObj = weatherJsonArray.getJSONObject(i);
                resultObj = resultObj.getJSONObject("Temperature");
                String HourTemp = String.valueOf(Math.round((resultObj.getInt("Value") - 32) / 1.8));
                tvHourTemp.setText(HourTemp + "°");

                resultObj = weatherJsonArray.getJSONObject(i).getJSONObject("Wind").getJSONObject("Speed");
                String HourWindSpeed = String.valueOf(Math.round(resultObj.getInt("Value") * 1.61));
                tvHourWind.setText(HourWindSpeed + " км/ч");

                resultObj = weatherJsonArray.getJSONObject(i);
                String HourHumidity = String.valueOf(resultObj.getInt("RelativeHumidity"));
                String HourWeather = String.valueOf(resultObj.getString("IconPhrase"));
                tvHourHumidity.setText(String.valueOf(HourHumidity + "%"));
                tvHourWeather.setText(HourWeather);

                String uri = "drawable/R";
                String packageName = context.getPackageName();
                Log.i(TAG, uri);
                int image = context.getResources().getIdentifier(uri, "drawable", packageName);
                //int WeatherIcon = resultObj.getInt("WeatherIcon");

                SharedPreferences.Editor ed = sPref.edit();
                ed.putString("HOUR_TEMPERATURE", HourTemp);
                ed.putString("HOUR_WIND_SPEED", HourWindSpeed);
                ed.putString("HOUR_HUMIDITY", HourHumidity);
                ed.putString("HOUR_WEATHER", HourWeather);
                ed.apply();

                //Log.i(TAG, "parseJSON: data: " + date);
            }

            JSONObject weatherJsonObject = new JSONObject(JSONFiveDaysOfDailyForecasts);
            weatherJsonArray = weatherJsonObject.getJSONArray("DailyForecasts");

            for (int i = 0; i < 3; i++) {
                JSONObject resultObj = weatherJsonArray.getJSONObject(i + 1);
                String date = formatDates(resultObj.getString("Date"));
                long tempMin = Math.round((resultObj.getJSONObject("Temperature").getJSONObject("Minimum").getInt("Value") - 32) / 1.8);
                long tempMax = Math.round((resultObj.getJSONObject("Temperature").getJSONObject("Maximum").getInt("Value") - 32) / 1.8);
                String icon = String.format("%02d", resultObj.getJSONObject("Day").getInt("Icon"));
                Log.i (TAG, "JSONFiveDaysOfDailyForecasts: IconNumbers: " + icon);

                String weather = resultObj.getJSONObject("Day").getString("IconPhrase");
                TextView tvWeatherDay = tvW.get(i);
                TextView tvTempDay = tvW.get(i + 3);
                TextView tvDay = tvW.get(i + 6);
                tvDay.setText(date);
                tvWeatherDay.setText(weather);
                tvTempDay.setText(String.format("%d°/%d°",  tempMin , tempMax));

                Picasso.with(this)
                        .load("https://developer.accuweather.com/sites/default/files/" + icon + "-s.png")
                        .error(R.drawable.ic_weather_cloudy)
                        .into(ivW.get(i));

                SharedPreferences.Editor ed = sPref.edit();
                ed.putString("DAY_TEMPERATURE_MIN" + i, String.valueOf(tempMin));
                ed.putString("DAY_TEMPERATURE_MAX" + i, String.valueOf(tempMax));
                ed.putString("DAY_WEATHER" + i, String.valueOf(weather));
                ed.putString("DAY_DAY" + i, String.valueOf(date));
                ed.putString("DAY_ICON" + i, String.valueOf(icon));
                ed.apply();

            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }



    private String loadSettings(String key, String defValue) {
        sPref = getSharedPreferences("MainPreferences", MODE_PRIVATE);
        String saved = sPref.getString(key, defValue);
        Log.i (TAG, "SharedPreferencesLoad: " + key + ": OK");
        return saved;
    }

    private void saveLocationKey(String key,String namekey) {
        sPref = getSharedPreferences("MainPreferences", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(key, namekey);
        ed.apply();
        Log.i (TAG, "SharedPreferencesSave: " + key + ": OK");
    }


    private void closeActivity() {
        this.finish();
    }

    private void weatherRecovery() {
        sPref = getSharedPreferences("WeatherPreferences", MODE_PRIVATE);
        tvHourTemp.setText(String.format("%s°", sPref.getString("HOUR_TEMPERATURE", "0")));
        tvHourWind.setText(String.format("%s км/ч", sPref.getString("HOUR_WIND_SPEED", "0")));
        tvHourHumidity.setText(String.valueOf(sPref.getString("HOUR_HUMIDITY", "0") + "%"));
        tvHourWeather.setText(sPref.getString("HOUR_WEATHER", "Неизвестно"));

        for (int i = 0; i < 3; i++) {
            TextView tvWeatherDay = tvW.get(i);
            TextView tvTempDay = tvW.get(i + 3);
            TextView tvDay = tvW.get(i + 6);
            tvDay.setText(sPref.getString("DAY_DAY" + i, "0"));
            tvWeatherDay.setText(sPref.getString("DAY_WEATHER" + i, "0"));
            tvTempDay.setText(String.format("%s°/%s°", sPref.getString("DAY_TEMPERATURE_MIN" + i, "0"), sPref.getString("DAY_TEMPERATURE_MAX" + i, "0")));


            Picasso.with(this)
                    .load("https://developer.accuweather.com/sites/default/files/" + sPref.getString("DAY_ICON" + i, "0") + "-s.png")
                    .error(R.drawable.ic_weather_cloudy)
                    .into(ivW.get(i));
        }


    }

    public String formatDates(String DateTime) throws ParseException {
        String[] date = DateTime.split("T");
        SimpleDateFormat oldDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        Date dates = oldDateFormat.parse(date[0]);
        String result = newDateFormat.format(dates);

        dates = newDateFormat.parse(result);
        newDateFormat.applyPattern("EEEE");
        result = newDateFormat.format(dates);

        return result;
    }
}