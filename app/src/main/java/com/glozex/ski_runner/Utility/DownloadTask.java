package com.glozex.ski_runner.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.glozex.ski_runner.Activities.SettingsActivity;
import com.glozex.ski_runner.Model.CheckForSDCard;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadTask {

    private static final String TAG = DownloadTask.class.getSimpleName();
    private Context context;
    private String downloadUrl, downloadFileName;
    private String SDPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    private String zipPath = SDPath + "/Ski-Runner DLC/" ;
    private String unzipPath = SDPath + "/Ski-Runner DLC/";

    public DownloadTask(Context context, String downloadUrl) {
        this.context = context;
        this.downloadUrl = downloadUrl;

        downloadFileName = downloadUrl.replace(DownloadLinks.mainUrl, "");//Create file name by picking download file name from URL
        Log.i(TAG, downloadFileName);

        new DownloadingTask().execute();
    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;
        Dialog progressdlc;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressdlc = ProgressDialog.show(context, "Скачивание пакета", "Подождите несколько минут...");
        }

        @Override
        protected void onPostExecute(Void result) {
            progressdlc.dismiss();

            try {
                if (outputFile != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Готово")
                            .setMessage("Дополнительный контент был успешно скаченн")
                            .setCancelable(false)
                            .setPositiveButton("Установить",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            progressdlc = ProgressDialog.show(context, "Установка", "Подождите пожалуйста");
                                            InstallDLC.unzip(zipPath + "images.zip",unzipPath);
                                            progressdlc.dismiss();
                                            Intent intent = new Intent(context, SettingsActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            context.startActivity(intent);
                                            ((Activity)context).finish();
                                            dialog.cancel();
                                        }
                                    }).setNegativeButton("Установить позже",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog("Ошибка", "Не удалось скачать пакет.", "Ок");
                        }
                    }, 3000);

                    Log.i(TAG, "Download Failed");

                }
            } catch (Exception e) {
                e.printStackTrace();
                AlertDialog("Ошибка", "Не удалось скачать пакет.", "Ок");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog("Ошибка", "Не удалось скачать пакет.", "Ок");
                    }
                }, 3000);
                Log.i(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }


            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(downloadUrl);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.connect();

                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.i(TAG, "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }

                if (new CheckForSDCard().isSDCardPresent()) {

                    apkStorage = new File(
                            Environment.getExternalStorageDirectory() + "/"
                                    + DownloadLinks.downloadDirectory);
                } else
                    Toast.makeText(context, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.i(TAG, "Directory Created.");
                }

                outputFile = new File(apkStorage, downloadFileName);

                if (!outputFile.exists() && c.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    outputFile.createNewFile();
                    Log.i(TAG, "File Created");
                }

                if (c.getResponseCode() == HttpURLConnection.HTTP_OK){
                    FileOutputStream fos = new FileOutputStream(outputFile);

                    InputStream is = c.getInputStream();

                    byte[] buffer = new byte[1024];
                    int len1 = 0;
                    while ((len1 = is.read(buffer)) != -1) {
                        fos.write(buffer, 0, len1);
                    }

                    fos.close();
                    is.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
                outputFile = null;
                Log.i(TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }

        public void AlertDialog(String setTitle, String setMessage, String setPositiveButtonTitle) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(setTitle)
                    .setMessage(setMessage)
                    .setCancelable(false)
                    .setPositiveButton(setPositiveButtonTitle,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    /*Intent intent = new Intent(context, DlcDownloadActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(intent);
                                    ((Activity)context).finish();*/
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }
}
