package com.glozex.ski_runner.Utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class AlertDialogBuilder {

    public String Title;
    public String Message;
    public Context context;

    public AlertDialogBuilder(String title, String message, Context context) {
        this.Title = title;
        this.Message = message;
        this.context = context;
    }

    public void ConclusionAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(Title)
                .setMessage(Message)
                .setCancelable(false)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}