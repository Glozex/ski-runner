package com.glozex.ski_runner.Utility;

public class DownloadLinks {
    public static final String downloadDirectory = "Ski-Runner DLC";
    public static final String mainUrl = "https://ski-runnerdlc.000webhostapp.com/";
    public static final String downloadZipUrl = "https://ski-runnerdlc.000webhostapp.com/images.zip";
}
