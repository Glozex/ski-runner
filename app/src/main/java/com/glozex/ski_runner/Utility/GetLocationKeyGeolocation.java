package com.glozex.ski_runner.Utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.glozex.ski_runner.Activities.WeatherActivity;
import com.glozex.ski_runner.Helpers.GetHTTPDataHelper;
import com.glozex.ski_runner.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

import static android.content.Context.MODE_PRIVATE;

public class GetLocationKeyGeolocation extends AsyncTask<URL,Void,String> {
    private static final String TAG = GetLocationKeyGeolocation.class.getSimpleName();
    private LocationManager locationManager;
    private locationListener LocationListener;
    private Context ContextAsync;

    public static String City;
    public String locationkey = "";
    public URL WeatherURL;

    SharedPreferences sPref;
    Dialog progress;
    double lat=0.0;
    double lng=0.0;

    public GetLocationKeyGeolocation(Context context){
        this.ContextAsync = context;
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        LocationListener = new locationListener();
        locationManager = (LocationManager) ContextAsync.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, LocationListener);
        progress = ProgressDialog.show(ContextAsync, ContextAsync.getResources().getString(R.string.GetLocationKeyGeolocationProgressDialog), ContextAsync.getResources().getString(R.string.MessageProgressDialog_1));
    }

    @SuppressLint("MissingPermission")
    @Override
    protected String doInBackground(URL... urls) {
        String result;
        while (this.lat == 0.0) {}

        while (locationkey.isEmpty()) {
            if (WeatherURL != null)
            {
                GetHTTPDataHelper weatherhelper = new GetHTTPDataHelper();
                result = weatherhelper.getHTTPData(WeatherURL);
                Log.i(TAG, "doInBackground: JSON: " + result);
                return result;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        result = LocationKeyJSON(result);
        saveLocationKey(result, City);
        Log.i(TAG, "onPostExecute: Key: " + result);
        Log.i(TAG, "onPostExecute: City: " + City);
        AlertDialog();
    }
    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    public class locationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {

            try {
                lat = location.getLatitude();
                lng = location.getLongitude();
                Log.i(TAG, "Latitude: " + lat);
                Log.i(TAG, "Longitude: " + lng);
                String URLGeopositionSearch = "http://dataservice.accuweather.com/locations/v1/cities/geoposition/search";
                GetHTTPDataHelper GeopositionSearch = new GetHTTPDataHelper();
                GeopositionSearch.setWeatherBaseUrl(URLGeopositionSearch);
                WeatherURL = GeopositionSearch.buildURLForGPSLocation(String.valueOf(lat),String.valueOf(lng));
                progress.dismiss();
                progress = ProgressDialog.show(ContextAsync, ContextAsync.getResources().getString(R.string.GetLocationKeyGeolocationProgressDialog), ContextAsync.getResources().getString(R.string.GetCoordinatesProgressDialog));
                locationManager.removeUpdates(this);
            } catch (Exception e) {}
        }

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    }

    private String LocationKeyJSON (String result) {
        if (locationkey != null) {
            locationkey = "";
        }

        if (result != null){
            try {
                JSONObject weatherJsonObject = new JSONObject(result);
                String key = weatherJsonObject.getString("Key");
                City = weatherJsonObject.getString("LocalizedName");
                progress.dismiss();
                return key;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void AlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContextAsync);
        builder.setTitle("Готово")
                .setMessage("Ваш город: " + GetLocationKeyGeolocation.City + "?")
                .setCancelable(false)
                .setPositiveButton("Да",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(ContextAsync, WeatherActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                ContextAsync.startActivity(intent);
                                ((Activity)ContextAsync).finish();
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("Нет",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SharedPreferences preferences = ContextAsync.getSharedPreferences("MainPreferences", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                editor.apply();
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void saveLocationKey(String key, String city) {
        sPref = ContextAsync.getSharedPreferences("MainPreferences", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("KEY_CITY", key);
        ed.putString("NAME_CITY", city);
        ed.apply();
        Log.i (TAG, "SharedPreferences: saveLocationKey: " + sPref.getString("KEY_CITY", ""));
    }
}