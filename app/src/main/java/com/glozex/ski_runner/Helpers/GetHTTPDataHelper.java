package com.glozex.ski_runner.Helpers;

import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class GetHTTPDataHelper {
    private static final String TAG = GetHTTPDataHelper.class.getSimpleName();
    private static String WEATHER_BASE_URL = null;
    private static final String API_KEY = "TuHdWhHWTMK91GKteggH7ywaHTACDd33";
    private static final String PARAM_API_KEY = "apikey";
    private static final String API_LANGUAGE = "ru";
    private static final String API_DETAILS = "true";

    static String jsonWeather = null;

    public GetHTTPDataHelper () {}

    // http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=GDEKQiwLVuaiLs6agurgyJZnj4ZZlktR&q=52.2836232,104.247135&language=ru

    public URL buildURLForGPSLocation(String lat, String lon) {
        URL url = null;
        Uri buildUri = Uri.parse(WEATHER_BASE_URL).buildUpon()
                .appendQueryParameter(PARAM_API_KEY, API_KEY)
                .appendQueryParameter("q", lat + "," + lon)
                .appendQueryParameter("language", API_LANGUAGE)
                .appendQueryParameter("details", API_DETAILS)
                .build();
        try {
            url = new URL(buildUri.toString());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Log.i(TAG, "Helper: " + url);
        return url;
    }

    // http://dataservice.accuweather.com/forecasts/v1/daily/5day/292712?apikey=GDEKQiwLVuaiLs6agurgyJZnj4ZZlktR

    public URL buildURLForWeather(String type, String time, String locationKey) {
        URL url = null;
        Uri buildUri = Uri.parse(WEATHER_BASE_URL).buildUpon()
                .appendEncodedPath(type + "/" + time + "/" + locationKey)
                .appendQueryParameter(PARAM_API_KEY, API_KEY)
                .appendQueryParameter("language", API_LANGUAGE)
                .appendQueryParameter("details", API_DETAILS)
                .build();
        try {
            url = new URL(buildUri.toString());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Log.i(TAG, "buildURLForWeather: " + url);
        return url;
    }

    public String getHTTPData(URL url){
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setConnectTimeout(5000);
            if(httpURLConnection.getResponseCode() == 200)
            {
                BufferedReader r = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while((line = r.readLine())!=null)
                    sb.append(line);
                jsonWeather = sb.toString();
                httpURLConnection.disconnect();
            }
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return jsonWeather;
    }

    public static void setWeatherBaseUrl(String weatherBaseUrl) {
        WEATHER_BASE_URL = weatherBaseUrl;
    }
}

