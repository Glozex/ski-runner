package com.glozex.ski_runner.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.glozex.ski_runner.R;

import java.io.File;

public class CustomAdapterListview extends SimpleCursorAdapter {
    private static final String TAG = CustomAdapterListview.class.getSimpleName();
    private String SDPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    private Cursor c;

    public CustomAdapterListview(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        this.c = c;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_of_ointments, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        String Brand = this.c.getString(this.c.getColumnIndex("Brand"));
        String Title = this.c.getString(this.c.getColumnIndex("Title"));
        int Price = this.c.getInt(this.c.getColumnIndex("Price"));
        String imgid = this.c.getString(this.c.getColumnIndex("Image"));
        String packageName = context.getPackageName();
        File checkFile = new File(String.format("%s/Ski-Runner DLC/%s", SDPath, "images.ch"));
        ImageView iv = (ImageView) view.findViewById(R.id.lv_image);

        if (checkFile.exists()) {
            if (imgid != null) {
                File imgFile = new File(String.format("%s/Ski-Runner DLC/%s.jpg", SDPath, imgid));
                Log.i(TAG, "ImageDir: " + imgFile.getPath());
                Log.i(TAG, "ImageID: " + imgid);

                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        iv.setImageBitmap(myBitmap);
                    } else {
                        iv.setImageResource(R.drawable.ic_launcher_background);
                    }
            }
        }

        else
        {
            iv.setVisibility(View.GONE);
        }

        TextView tvBrand = (TextView) view.findViewById(R.id.lv_manufacturers);
        TextView tvTitle = (TextView) view.findViewById(R.id.lv_ointment);
        TextView tvPrive = (TextView) view.findViewById(R.id.lv_price);
        tvBrand.setText("Производитель: " + Brand);
        tvTitle.setText(Title);
        tvPrive.setText(Price + " руб.");
    }
}
